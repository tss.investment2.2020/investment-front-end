import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { delay  } from 'rxjs/operators';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage  = false;
  hiddenProgress  = false;
  get email(): any{
    // tslint:disable-next-line: no-string-literal
    return this.loginForm.get('email');
  }
  get password(): any{
    // tslint:disable-next-line: no-string-literal
    return this.loginForm.get('password');
  }
  public errorsMessages = {
    email: [
      { type: 'required', message: 'Email es requerido' },
      { type: 'maxlength', message: '100 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    password: [
      { type: 'required', message: 'Password es requerido' },
      { type: 'maxlength', message: '8 caracteres como maximo' },
      { type: 'minlength', message: '6 caracteres como minimo' }
    ]
  };
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private authService: AuthService) {
                this.buildForm();
              }

  ngOnInit(): void {
  }

  buildForm(): any {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(100), Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.maxLength(8), Validators.minLength(6)]]
    });
  }
  login(): any {
    this.hiddenProgress = true;
    // return this.authService.login(this.loginForm.value).subscribe(
    //   res => {
    //     console.log(res);
    const token = '1';
    localStorage.setItem('Auth_token', token);
    this.router.navigateByUrl('/core').then(() => {
      window.location.reload();
    });
    //   }, error => {
    //     this.errorMessage = true;
    //     this.hiddenProgress  = false;
    //     console.log(error);
    //   }
    // );
  }
}
