import { TestBed } from '@angular/core/testing';

import { HttpyService } from './httpy.service';

describe('HttpyService', () => {
  let service: HttpyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
