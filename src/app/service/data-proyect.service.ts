import { Injectable } from '@angular/core';
import { HttpyService } from './httpy.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataProyectService {

  constructor(private httpyClient: HttpyService) {  }

  saveNameProyect(proyectForm: any): Observable<any> {
    return this.httpyClient.post('proyect', proyectForm);
  }
  saveDataFirstProyect(dataFirstForm: any): Observable<any> {
    return this.httpyClient.post('dataProyect', dataFirstForm);
  }
  saveDataSecondProyect(dataSecondForm: any): Observable<any> {
    return this.httpyClient.post('secondDataProyect', dataSecondForm);
  }
  saveFlujoDataSecondProyect(flujoNetoForm: any): Observable<any> {
    return this.httpyClient.post('flujoSecondDataProyect', flujoNetoForm);
  }
  getFirstResult(idData): Observable<any> {
    return this.httpyClient.get('result-data-first/' + idData);
  }
  getFirstData(idData): Observable<any> {
    return this.httpyClient.get('first-data/' + idData);
  }
  getSecondData(idProyect): Observable<any> {
    return this.httpyClient.get('second-data/' + idProyect);
  }
  getSecondResult(idProy): Observable<any> {
    return this.httpyClient.get('result-data-second/' + idProy);
  }
  getFlujoSecondResult(idProy): Observable<any> {
    return this.httpyClient.get('flujo-second-data/' + idProy);
  }
  getNameProyect(idProyect): Observable<any> {
    return this.httpyClient.get('name-proyect/' + idProyect);
  }
}
