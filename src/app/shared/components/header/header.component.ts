import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from 'src/app/login/login.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  dialogRef: any;
  emptyTokken = true;
  hiddenLogBtn = false;
  constructor(private dialog: MatDialog,
              private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('Auth_tokken') == null)  {
      this.emptyTokken = false;
      this.hiddenLogBtn = true;
    }else {
      this.emptyTokken  = true;
      this.hiddenLogBtn = false;
    }
  }
  openLogin(): any {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '480px',
      height: 'auto'
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }
  logout(): any {
    localStorage.clear();
    this.router.navigateByUrl('').then(() => {
      window.location.reload();
    });
  }
}
