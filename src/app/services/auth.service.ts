import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { delay, mergeMap} from 'rxjs/operators';
import { fromEvent, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpService: HttpService) { }

  login(loginForm: any): Observable<any> {
    return this.httpService.post('auth/login', loginForm);
  }
  register(signUpForm: any): Observable<any> {
    return this.httpService.post('users', signUpForm);
  }
}
