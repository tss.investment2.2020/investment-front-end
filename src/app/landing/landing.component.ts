import { Component, OnInit } from '@angular/core';

interface SvgLanding {
  src: string;
}
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  svgs: SvgLanding[] = [
    {src: './assets/image/landing-svg/chart-1.svg'},
    {src: './assets/image/landing-svg/chart-2.svg'},
    {src: './assets/image/landing-svg/chart-3.svg'},
    {src: './assets/image/landing-svg/screen-02.svg'},
    {src: './assets/image/landing-svg/chart-4.svg'},
    {src: './assets/image/landing-svg/screen-02-1.svg'},
    {src: './assets/image/landing-svg/screen-02-4.svg'},
    {src: './assets/image/landing-svg/screen-02-1-2.svg'},
    {src: './assets/image/landing-svg/screen-02-2.svg'},
  ];

  constructor() { }

  ngOnInit(): void {

  }

}
