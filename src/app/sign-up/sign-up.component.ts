import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  get firsName(): any {
    return this.signUpForm.get('name');
  }
  get lastName(): any {
    return this.signUpForm.get('lastName');
  }
  get password(): any {
    return this.signUpForm.get('password');
  }
  get confirmPassword(): any {
    return this.signUpForm.get('confirmPassword');
  }
  get email(): any {
    return this.signUpForm.get('email');
  }
  get phone(): any {
    return this.signUpForm.get('phone');
  }
  public errorsMessages = {
    firstName: [
      { type: 'required', message: 'Nombre(s) es requerido' },
      { type: 'maxlength', message: '40 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' },
      { type: 'pattern', message: 'solo se permiten caracteres' }
    ],
    lastName: [
      { type: 'required', message: 'Apellido(s) es requerido' },
      { type: 'maxlength', message: '20 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' },
      { type: 'pattern', message: 'solo se permiten caracteres' }
    ],
    password: [
      { type: 'required', message: 'Contraseña es requerido' },
      { type: 'maxlength', message: '8 caracteres como maximo' },
      { type: 'minlength', message: '6 caracteres como minimo' },
    ],
    confirmPassword: [
      { type: 'required', message: 'Contraseña es requerido' }
    ],
    email: [
      { type: 'required', message: 'Email es requerido' },
      { type: 'email', message: 'Ingrese un correo electronico valido' }
    ],
    phone: [
      { type: 'nullValidator', message: '' },
      { type: 'maxlength', message: '8 digitos como maximo' },
      { type: 'minlength', message: '8 digitos como minimo' },
      { type: 'pattern', message: 'Solo se permiten numeros' }
    ]
  };
  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private dialog: MatDialog) {
    this.buldForm();
  }

  ngOnInit(): void {
  }
  buldForm(): any {
    this.signUpForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, { validator: this.checkPasswords });
  }
  checkPasswords(group: FormGroup): any {
    const contraseña = group.controls.password.value;
    const confirmarContraseña = group.controls.confirmPassword.value;
    return contraseña === confirmarContraseña ? null : { notSame: true };
  }
  register(): any {
    console.log(this.signUpForm.value);
    this.authService.register(this.signUpForm.value).subscribe(
      res => {
        if (res == null) {
          this.signUpForm.reset();
          this.openLogin();
        }
      }, error => {
        console.log(error);
      }
    );
  }
  openLogin(): any {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '480px',
      height: 'auto'
    });
  }
}
