import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultComponent } from './result.component';

const routes: Routes = [
  {
    path: '',
    component: ResultComponent
  },
  {
    path: 'proyect-result',
    loadChildren: () => import('./proyect-result/proyect-result.module').then( m => m.ProyectResultModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultRoutingModule { }
