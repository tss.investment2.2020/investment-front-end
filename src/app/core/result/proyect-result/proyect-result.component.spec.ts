import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectResultComponent } from './proyect-result.component';

describe('ProyectResultComponent', () => {
  let component: ProyectResultComponent;
  let fixture: ComponentFixture<ProyectResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
