import { Component, OnInit, ViewChild } from '@angular/core';
import { DataProyectService } from 'src/app/service/data-proyect.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { PdfMakeWrapper, Img, Table } from 'pdfmake-wrapper';
import { ITable } from 'pdfmake-wrapper/lib/interfaces';
import * as html2pdf from 'html2pdf.js';
import { Router } from '@angular/router';

export interface FlujoNeto {
  flujo: number;
  anio: number;
}
export interface SecondResult {
  index: number;
  inv_ini: number;
  anio_1: number;
  anio_2: number;
  anio_3: number;
  anio_4: number;
  anio_5: number;
  VPN: number;
  inflacion: number;
}
export interface SecondData {
  id: number;
  id_proyc: number;
  tipo: string;
  est_pes: number;
  est_prob: number;
  est_opt: number;
  yeear: number;
}
export interface DisplaySecondResult {
  name: string;
}
@Component({
  selector: 'app-proyect-result',
  templateUrl: './proyect-result.component.html',
  styleUrls: ['./proyect-result.component.css']
})
export class ProyectResultComponent implements OnInit {

  public lineChartData: ChartDataSets[] = [];
  public lineChartSecondData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,1)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public barChartType: ChartType = 'bar';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  tir: number;
  aprove: boolean;
  denied: boolean;
  nameProyect: any;
  year: number;

  displayedColumns = ['anio', 'flujo'];
  displayedSecondColumns = ['tipo', 'est_pes', 'est_prob', 'est_opt'];
  displayedSecondColumnsResult: DisplaySecondResult[] = [];
  displayedSecondColumnsResultAux = [];

  flujoNeto: FlujoNeto[];
  secondData: SecondData[];
  secondResultData: SecondResult[];
  vpn = [];

  inversion: any;
  firstOption = false;
  secondOption = false;
  prom: number;
  constructor(private dataProyectService: DataProyectService,
              private router: Router) { }

  ngOnInit(): void {
    this.verifyEscenary();
  }
  verifyEscenary(): any {
    const idResult = localStorage.getItem('id_dataFirst');
    const idProyect = localStorage.getItem('id_proy');
    const option = localStorage.getItem('option');
    // tslint:disable-next-line: triple-equals
    this.dataProyectService.getNameProyect(idProyect).subscribe(
      res => {
        this.nameProyect = res.name;
        // console.log(res);
      }, error => {
        console.log(error);
      }
      );
    // tslint:disable-next-line: triple-equals
    if (option == 'first') {
      this.firstOption = true;
      this.dataProyectService.getFirstResult(idResult).subscribe(
        res => {
          this.buildTableFlujo(res.fjNeto, res.year);
          this.inversion = res.invertion;
          this.tir = res.tir * 100;
          if (this.tir > 30) {
            this.aprove = true;
          }else {
            this.denied = true;
          }
          console.log(res);
          console.log(this.tir);
        }, error => {
          console.log(error);
        }
      );
      this.dataProyectService.getFirstData(idResult).subscribe(
        res => {
          this.year = res.year;
          console.log(res);
        }, error => {
          console.log(error);
        }
      );
    }else {
      this.secondOption = true; 
      this.dataProyectService.getSecondData(idProyect).subscribe(
        res => {
          this.secondData = res;
          // this.buildTableSecondData(res);
        }, error => {
          console.log(error);
        }
      );
      this.dataProyectService.getSecondResult(idProyect).subscribe(
        res => {
          this.displayedSecondColumnsResult = res.schema.fields;
          this.secondResultData = res.data;
          // console.log(res.data);
          this.buildLineSecondChart(res.data);
          this.buildSecondColumns(res.schema.fields);
        }, error => {
          console.log(error);
        }
      );
      this.dataProyectService.getFlujoSecondResult(idProyect).subscribe(
        res => {
          console.log(res);
        }, error => {
          console.log(error);
        }
      );
    }
  }
  buildLineSecondChart(datas: any) {
    let arrayAux = [];
    for (const data of  datas) {
      // console.log(data.VPN);
      arrayAux.push(data.VPN);
    }
    this.vpn = arrayAux;
    const suma = this.vpn.reduce((previous, current) => current += previous);
    this.prom = suma / this.vpn.length;
    if (this.prom > 0) {
      this.aprove = true;
    }else {
      this.denied = true;
    }
    console.log("Premdio  VPN:" + this.prom);
    const newLineChartData = {
      data: this.vpn,
      label: 'VPN'
    };
    this.lineChartData.push(newLineChartData);
    console.log(this.lineChartData);
    console.log(this.vpn);
    for (let i = 0; i <= 100; i++) {
      const j = i + 1;
      this.lineChartLabels.push("i:" + j);
    }
  }
  buildSecondColumns(fields: any) {
    let arrayAux = []
    for(const field of fields){
      const name: string = field.name;
      arrayAux.push(name);
    }
    this.displayedSecondColumnsResultAux = arrayAux;
    arrayAux =  [];
    // console.log(this.displayedSecondColumnsResultAux);
  }
  buildTableSecondData(secondData: any) {
    // console.log(secondData);
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < secondData.length; i++) {
      const data = secondData[i];
      this.secondData.push(data);
    }
    console.log(this.secondData);
  }
  buildTableFlujo(fjNeto, year: number): any {
    const arrayAux = [];
    const arraAuxSecond = [];
    for (let i = 0; i < fjNeto.length; i++) {
      const flujo = {
        anio: i + 1,
        flujo: fjNeto[i]
      };
      const label = 'Año ' + i + 1;
      arraAuxSecond.push(label);
      arrayAux.push(flujo);
      const j = i + 1;
      this.lineChartLabels.push('Año ' + j);
    }
    this.flujoNeto = arrayAux;
    console.log(this.flujoNeto);
    const newLineChartData = {
      data: fjNeto,
      label: 'Flujo neto'
    };
    this.lineChartData.push(newLineChartData);
    console.log(this.lineChartLabels);
  }
  async generatePdf() {
    const option = localStorage.getItem("option");
    if (option == 'first') {
      var element = document.getElementById('element-to-print');
      html2pdf(element);
    }else{
      var element2 = document.getElementById('second-element-to-print');
      html2pdf(element2);
    }
    // const pdf = new PdfMakeWrapper();
    // pdf.add('INVESTMENT SOFT');
    // // pdf.add( await new Img('').build());
    // pdf.add('---------------------------------------');
    // pdf.add('Nombre de Proyecto: ' + this.nameProyect);
    // pdf.add('---------------------------------------');
    // pdf.add('Tasa Interna de Retorno: ' + this.tir + ' %');
    // pdf.add('---------------------------------------');
    // pdf.add('Inversión del Proyecto:  ' + this.inversion + ' $');
    // pdf.add('---------------------------------------');
    // pdf.add('Flujo neto en el periodo t =  ' + this.year + ' años');
    // pdf.add('---------------------------------------');
    // pdf.add(this.createTablePdf(this.flujoNeto)); 
    // pdf.create().open();
  }
  createTablePdf(data: FlujoNeto[]): ITable {
    [{ }]
    return new Table([
      ['Año', 'Flujo Neto'],
      ...this.extractDataTable(data)
    ]).end;
  }
  extractDataTable(data: FlujoNeto[]){
    return data.map(row => [row.anio, row.flujo]);
  }
  goToHome(){
    this.router.navigateByUrl('core/result/proyect-result', { skipLocationChange: true }).then(() => {
      this.router.navigate(['core/home']);
    });
  }
}
