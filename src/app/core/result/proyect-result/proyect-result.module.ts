import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProyectResultRoutingModule } from './proyect-result-routing.module';
import { ProyectResultComponent } from './proyect-result.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';

import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [ProyectResultComponent],
  imports: [
    CommonModule,
    ProyectResultRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    ChartsModule
  ]
})
export class ProyectResultModule {
 }
