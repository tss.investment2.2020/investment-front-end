import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  hiddenEmpty = false;
  hiddenContent = true;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  home(): any {
    this.router.navigateByUrl('/core/home').then(() => {
      window.location.reload();
    });
  }

}
