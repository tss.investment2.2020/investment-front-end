import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ResultComponent } from './result/result.component';
import { MaterialModule } from '../material-module';
import { CoreComponent } from './core.component';
import { CoreRoutingModule } from './core-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    HomeComponent,
    ResultComponent,
    CoreComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    CoreRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    ResultComponent
  ],
  exports: [
    HomeComponent,
    ResultComponent
  ]
})
export class CoreModule { }
