import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ResultComponent } from './result/result.component';
import { CoreComponent } from './core.component';
const routes: Routes = [
  {
    path: 'core',
    component: CoreComponent,
    children: [
      {
        path: 'home', component: HomeComponent
      },
      {
         path: 'result',
         loadChildren: () => import('./result/result.module').then( m => m.ResultModule)
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  }
];
export const appRouting = RouterModule.forRoot(routes);
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class CoreRoutingModule { }
