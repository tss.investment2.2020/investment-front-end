import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.css']
})
export class CoreComponent implements OnInit {
  links = [
    {
      label: 'HOME',
      link: './home',
      index: 0,
      icon: 'home'
    },
    {
      label: 'RESULT',
      link: './result',
      index: 1,
      icon: 'insights'
    },
  ];
  activeLink = this.links[0];

  constructor() { }

  ngOnInit(): void {
  }
}
