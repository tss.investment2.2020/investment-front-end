import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { DataProyectService } from 'src/app/service/data-proyect.service';
import { Router } from '@angular/router';

interface InputForm {
  type: string;
  label: string;
  formControlName: string;
}

/**
 * @title Stepper that displays errors in the steps
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class HomeComponent implements OnInit {
  hiddenEsceSecond = false;
  hiddenEsceFirst = false;
  objects: Array<any> = [];
  select = '';
  inputs: InputForm[] = [
    {type: 'text', label: '($)', formControlName: 'act-fijo-pes'},
    {type: 'text', label: '($)', formControlName: 'act-fijo-prob'},
    {type: 'text', label: '($)', formControlName: 'act-fijo-op'},
    {type: 'text', label: '($)', formControlName: 'act-cir-pes'},
    {type: 'text', label: '($)', formControlName: 'act-cir-prob'},
    {type: 'text', label: '($)', formControlName: 'act-cir-op'},
    {type: 'text', label: '($)', formControlName: 'flu-imp-pes'},
    {type: 'text', label: '($)', formControlName: 'flu-imp-pro'},
    {type: 'text', label: '($)', formControlName: 'flu-imp-op'},
  ];
  options = [
    {label: '1', value: '1'},
    {label: '2', value: '2'},
    {label: '3', value: '3'},
    {label: '4', value: '4'},
    {label: '5', value: '5'},
    {label: '6', value: '6'},
    {label: '7', value: '7'},
    {label: '8', value: '6'},
    {label: '9', value: '8'},
    {label: '10', value: '10'},
  ];

  proyectForm: FormGroup;
  dataFormFirst: FormGroup;
  dataFormSecond: FormGroup;


  investForm: FormGroup;
  valorRescateForm: FormGroup;
  inflacionForm: FormGroup;
  flujoNetoForm: FormGroup;

  arrayFlujoForm = [];
  // tslint:disable-next-line: variable-name
  id_user: string;
  idProy: string;
  constructor(private formBuilder: FormBuilder,
              private dataProyectService: DataProyectService,
              private router: Router)
  {
    this.id_user = localStorage.getItem('Auth_token');

    this.buildForm();
  }

  buildForm(): any {
    this.proyectForm = this.formBuilder.group({
      id_user: [''],
      name: ['', [Validators.required]]
    });
    this.dataFormFirst = this.formBuilder.group({
      id_proyc: [''],
      media_first: ['', [Validators.required]],
      var_first: ['', [Validators.required]],
      media_second: ['', [Validators.required]],
      var_second: ['', [Validators.required]],
      year: ['', [Validators.required]],
    });
    this.investForm = this.formBuilder.group({
      id_proyc: [''],
      tipo:  ['invest'],
      est_pes: [''],
      est_prob: [''],
      est_opt: [''],
      year: [3]
    });
    this.valorRescateForm = this.formBuilder.group({
      id_proyc: [''],
      tipo:  ['rescate'],
      est_pes: [''],
      est_prob: [''],
      est_opt: [''],
      year: [3]
    });
    this.inflacionForm = this.formBuilder.group({
      id_proyc: [''],
      tipo:  ['tasa'],
      est_pes: [''],
      est_prob: [''],
      est_opt: [''],
      year: [3]
    });
    this.flujoNetoForm = this.formBuilder.group({
      id_proyc: [''],
      values: this.formBuilder.array([]) ,
    });
  }
  values() : FormArray {
    return this.flujoNetoForm.get('values') as FormArray
  }
  newValue(anio): FormGroup {
    return this.formBuilder.group({
      anio: [anio],
      flujo_neto: [''],
    })
  }
  ngOnInit(): void {
  }
  addValue(anio) {
    this.values().push(this.newValue(anio));
  }
  onSubmit() {
    this.idProy = localStorage.getItem('id_proy');
    this.flujoNetoForm.controls['id_proyc'].setValue(this.idProy);
    console.log(this.flujoNetoForm.value);
    this.dataProyectService.saveFlujoDataSecondProyect(this.flujoNetoForm.value).subscribe(
      res => {
        console.log(res);
      },error => {
        console.log(error);
      }
    );
  }
  saveName(e: Event): any {
    e.preventDefault();
    // tslint:disable-next-line: no-string-literal
    this.proyectForm.controls['id_user'].setValue(this.id_user);
    this.dataProyectService.saveNameProyect(this.proyectForm.value).subscribe(
      res => {
        localStorage.setItem('id_proy', res.id);
      }, error => {
        console.log(error);
      }
    );
  }

  getSelect(): void{
    let arrayForm: Array<any> = [];
    let arrayFormFujo: Array<any> = [];
    const i: number = + this.select;
    for (let j = 0 ; j < i ; j++) {
      this.addValue(j+1);
      // const object = {
      //   label: j + 1,
      //   formControlName: 'año',
      //   placeholder: 'Flujo Neto'
      // };
      // arrayForm.push(object);
      // const flujoForm: FormGroup = this.formBuilder.group({
      //   id_user: [''],
      //   año: [j + 1],
      //   flujo: [''],
      //   formControlName: ['flujo']
      // });
      // flujoForm.controls['id_user'].setValue(this.id_user);
      // arrayFormFujo.push(flujoForm);
    }
    this.select = '';
    this.objects = arrayForm;
    arrayForm = [];
    this.arrayFlujoForm = arrayFormFujo;
    arrayFormFujo = [];
  }

  changeEscenary(escenary: string): any{
    switch (escenary){
      case 'first':
        this.hiddenEsceFirst = true;
        this.hiddenEsceSecond = false;
        break;
      case 'second':
        this.hiddenEsceSecond = true;
        this.hiddenEsceFirst = false;
        break;
    }
  }
  getStart(e: Event): any{
    e.preventDefault();
    this.idProy = localStorage.getItem('id_proy');
    // tslint:disable-next-line: no-string-literal
    this.dataFormFirst.controls['id_proyc'].setValue(this.idProy);
    this.dataProyectService.saveDataFirstProyect(this.dataFormFirst.value).subscribe(
      res => {
        localStorage.setItem('id_dataFirst', res.id);
        localStorage.setItem('option', 'first');
        this.router.navigateByUrl('/core/home', { skipLocationChange: true }).then(() => {
          this.router.navigate(['core/result/proyect-result']);
        });
      }, error => {
        console.log(error);
      }
    );
  }
  startSimulation(): any {
    this.idProy = localStorage.getItem('id_proy');
    this.investForm.controls['id_proyc'].setValue(this.idProy);
    this.valorRescateForm.controls['id_proyc'].setValue(this.idProy);
    this.inflacionForm.controls['id_proyc'].setValue(this.idProy);
    console.log(this.investForm.value);
    console.log(this.valorRescateForm.value);
    console.log(this.inflacionForm.value);
    this.dataProyectService.saveDataSecondProyect(this.investForm.value).subscribe(
      res => {
        console.log(res);
      }, error => {
        console.log(error);
      }
    );
    this.dataProyectService.saveDataSecondProyect(this.valorRescateForm.value).subscribe(
      res2 => {
        console.log(res2);
      }, error => {
        console.log(error);
      }
    );
    this.dataProyectService.saveDataSecondProyect(this.inflacionForm.value).subscribe(
      res3 => {
        console.log(res3);
      }, error => {
        console.log(error);
      }
    );
    this.onSubmit();
    localStorage.setItem('option', 'second');
    this.router.navigateByUrl('/core/home', { skipLocationChange: true }).then(() => {
      this.router.navigate(['core/result/proyect-result']);
    });
  }
}
